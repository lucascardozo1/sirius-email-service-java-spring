package com.ldav.siriusemailservice.domain;

public enum Role {
    USER,
    ADMIN
}
